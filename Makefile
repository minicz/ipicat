SHELL := /bin/bash

package:
	python3 setup.py sdist bdist_wheel

upload:
	python3 -m twine upload --repository-url https://test.pypi.org/legacy/ dist/*

test_prepare:
	python3 -m venv /data/python/test
	source /data/python/test/bin/activate; \
	pip install -U jupyter; \
	pip install -U --index-url https://test.pypi.org/simple/ ipicat; \
	pip list | grep ipicat; \
	# jupyter notebook

test:
	source /data/python/test/bin/activate; \
	pip install -U --index-url https://test.pypi.org/simple/ ipicat; \
	pip list | grep ipicat; \
	jupyter notebook

run:
	cp /home/minicz/git/ipicat/ipicat/*.py /data/python/test/lib/python3.7/site-packages/ipicat/.
	source /data/python/test/bin/activate; \
	jupyter console


test_clean:
	rm -R /data/python/test

all:
	python3 setup.py sdist bdist_wheel
	python3 -m twine upload dist/*

clean:
	rm -R build/
	rm -R dist/
	rm -R ipicat.egg-info/
	rm -R ipicat/__pycache__/
	
